package io.mineparty.game.extras.rewards;

import com.google.common.base.Preconditions;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import io.mineparty.commons.bukkit.MinePartyCommons;
import io.mineparty.commons.core.MinePartyPlayer;
import io.mineparty.commons.core.data.Achievement;
import io.mineparty.commons.core.data.Statistic;
import io.mineparty.game.MPGame;
import io.mineparty.lib.util.CenteredText;
import io.mineparty.lib.util.PlayerUtil;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.*;

public class GameRewardManager {
    private final Map<UUID, PlayerReward> rewardMap = new HashMap<>();

    private PlayerReward getPlayerReward(UUID uuid) {
        return rewardMap.computeIfAbsent(uuid, PlayerReward::new);
    }

    public void addCoins(Player player, long coins) {
        Preconditions.checkNotNull(player, "player");
        Preconditions.checkArgument(coins > 0, "coins not greater than zero");

        PlayerReward reward = getPlayerReward(player.getUniqueId());
        Bukkit.getScheduler().runTaskAsynchronously(MPGame.getPlugin(), () -> MinePartyCommons.getPlugin().getPlayer(player).addCoins(coins));
        reward.coins += coins;
        player.sendMessage(ChatColor.GOLD + "+" + coins + ChatColor.WHITE + " coin" + (coins == 1 ? "s" : ""));
    }

    public void addReward(Player player, Reward reward) {
        Preconditions.checkNotNull(player, "player");
        Preconditions.checkNotNull(reward, "reward");

        PlayerReward playerReward = getPlayerReward(player.getUniqueId());
        playerReward.rewards.add(reward);

        MinePartyPlayer mpPlayer = MinePartyCommons.getPlugin().getPlayer(player);

        if (playerReward.coins > 0) {
            Bukkit.getScheduler().runTaskAsynchronously(MPGame.getPlugin(), () -> mpPlayer.addCoins(reward.coins));
            playerReward.coins += reward.coins;
            player.sendMessage(ChatColor.GOLD + "+" + playerReward.coins + ChatColor.WHITE + " coin" + (playerReward.coins == 1 ? "s" : ""));
        }

        if (reward.achievement.isPresent()) {
            PlayerUtil.addAchievement(player, reward.achievement.get(), ignored -> {});
        }

        // TODO: Statistics
        if (reward.statistic.isPresent()) {
            Bukkit.getScheduler().runTaskAsynchronously(MPGame.getPlugin(), () -> mpPlayer.increaseStatistic(reward.statistic.get(), 1));
        }
    }

    public int countRewards(Player player, Reward reward) {
        Preconditions.checkNotNull(player, "player");
        Preconditions.checkNotNull(reward, "reward");

        PlayerReward playerReward = getPlayerReward(player.getUniqueId());
        return playerReward.rewards.count(reward);
    }

    public void resetRewards() {
        rewardMap.clear();
    }

    public void sendRewards(Player player, int seconds) {
        int step = seconds / 3;

        Bukkit.getScheduler().runTaskLater(MPGame.getPlugin().getGameModeInformation().get().getPlugin(), () -> {
            PlayerReward reward = getPlayerReward(player.getUniqueId());
            CenteredText.sendCenteredMessage(player, "&6&lCoins");
            CenteredText.sendCenteredMessage(player, ChatColor.GRAY + "You earned " + ChatColor.GOLD + reward.coins + ChatColor.GRAY + " coin" + (reward.coins == 1 ? "s" : "") + " in this game.");
            player.sendMessage("");
        }, 20 * step);

        Bukkit.getScheduler().runTaskLater(MPGame.getPlugin().getGameModeInformation().get().getPlugin(), () -> {
            CenteredText.sendCenteredMessage(player, "&6&lStatistics");

            PlayerReward playerReward = getPlayerReward(player.getUniqueId());
            List<String> toSend = new ArrayList<>();
            for (Multiset.Entry<Reward> entry : playerReward.rewards.entrySet()) {
                String msg = ChatColor.GRAY + entry.getElement().getName() + ": " + ChatColor.GOLD + entry.getCount();
                toSend.add(msg);
            }

            if (toSend.isEmpty()) {
                CenteredText.sendCenteredMessage(player, ChatColor.GRAY + "Nothing... :(");
                player.sendMessage("");
            } else {
                int s = step / toSend.size();

                for (int i = 0; i < toSend.size(); i++) {
                    String m = toSend.get(i);
                    Bukkit.getScheduler().runTaskLater(MPGame.getPlugin(), () -> CenteredText.sendCenteredMessage(player, m), s * i);
                }

                Bukkit.getScheduler().runTaskLater(MPGame.getPlugin(), () -> player.sendMessage(""), s * toSend.size());
            }
        }, 20 * (step * 2));
    }

    @RequiredArgsConstructor
    private class PlayerReward {
        private final UUID uuid;
        private long coins = 0;
        private final Multiset<Reward> rewards = HashMultiset.create();
    }

    @Value
    @Builder
    public static class Reward {
        private final long coins;
        @NonNull
        private final Optional<Statistic> statistic;
        @NonNull
        private final Optional<Achievement> achievement;
        @NonNull
        private final String name;
    }
}
