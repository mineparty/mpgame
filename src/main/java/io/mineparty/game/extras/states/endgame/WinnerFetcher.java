package io.mineparty.game.extras.states.endgame;

import org.bukkit.entity.Player;

import java.util.Collection;

public interface WinnerFetcher {
    Collection<Player> getWinners();
    String getDescription();
}
