package io.mineparty.game.extras.states.endgame;

import com.google.common.collect.ImmutableList;
import lombok.experimental.UtilityClass;
import org.bukkit.entity.Player;

@UtilityClass
public class WinnerFetchers {
    public static WinnerFetcher nobody() {
        return new CollectionWinnerFetcher(ImmutableList.of());
    }

    public static WinnerFetcher forPlayer(Player player) {
        return new CollectionWinnerFetcher(ImmutableList.of(player));
    }
}
