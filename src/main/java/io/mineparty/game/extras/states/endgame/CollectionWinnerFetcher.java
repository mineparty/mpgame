package io.mineparty.game.extras.states.endgame;

import com.google.common.collect.ImmutableList;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionWinnerFetcher implements WinnerFetcher {
    private final List<Player> players;

    public CollectionWinnerFetcher(Collection<Player> players) {
        this.players = ImmutableList.copyOf(players);
    }

    @Override
    public Collection<Player> getWinners() {
        return players;
    }

    @Override
    public String getDescription() {
        if (players.isEmpty()) {
            return "Nobody";
        }

        return players.stream().map(Player::getName).collect(Collectors.joining(", "));
    }
}
