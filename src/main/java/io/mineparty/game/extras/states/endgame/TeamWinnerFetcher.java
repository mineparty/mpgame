package io.mineparty.game.extras.states.endgame;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.Collection;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class TeamWinnerFetcher implements WinnerFetcher {
    @NonNull
    private final Team team;

    @Override
    public Collection<Player> getWinners() {
        return team.getEntries().stream()
                .map(Bukkit::getPlayer)
                .filter(p -> p != null)
                .collect(Collectors.toList());
    }

    @Override
    public String getDescription() {
        return team.getDisplayName();
    }
}
