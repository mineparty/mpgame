package io.mineparty.game.extras.states;

import io.mineparty.game.GameState;
import io.mineparty.game.GameStatus;
import io.mineparty.game.MPGame;
import io.mineparty.lib.MPScoreboard;
import io.mineparty.lib.util.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

public class WaitingState implements GameState {
    private int timeUntilStart = -1;
    private final MPScoreboard scoreboard = new MPScoreboard(ChatColor.BOLD + "MINE" + ChatColor.LIGHT_PURPLE +
            ChatColor.BOLD + "PARTY");
    private BukkitTask tickTask;

    @Override
    public void onStart(GameState previous) {
        Bukkit.getOnlinePlayers().forEach(this::setupPlayer);
        Plugin plugin = MPGame.getPlugin().getGameModeInformation().get().getPlugin();
        tickTask = Bukkit.getScheduler().runTaskTimer(plugin, this::tick, 0, 20);
    }

    @Override
    public void onStop(GameState next) {

    }

    private void updateScoreboard() {
        scoreboard.add(ChatColor.AQUA.toString() + ChatColor.BOLD + "MAP", 5);
        scoreboard.add(ChatColor.GOLD + MPGame.getPlugin().getSelectedMap().getName(), 4);
        scoreboard.add(ChatColor.BLUE.toString(), 3);
        scoreboard.add(ChatColor.AQUA.toString() + ChatColor.BOLD + "PLAYERS", 2);
        int p = Bukkit.getOnlinePlayers().size();
        scoreboard.add(ChatColor.GREEN.toString() + p + ChatColor.GRAY + " player" + (p == 1 ? "" : "s") + " online", 1);

        if (timeUntilStart >= 0) {
            scoreboard.add(ChatColor.AQUA.toString() + ChatColor.BOLD + "STARTING IN", 8);
            scoreboard.add(ChatColor.GREEN.toString() + timeUntilStart + ChatColor.GRAY + " second" + (timeUntilStart == 1 ? "" : "s"), 7);
            scoreboard.add(ChatColor.RED.toString(), 6);
        } else {
            scoreboard.add(ChatColor.AQUA.toString() + ChatColor.BOLD + "WAITING FOR", 8);
            int m = MPGame.getPlugin().getSelectedMap().getMinPlayers() - Bukkit.getOnlinePlayers().size();
            String word = m == 1 ? "player" : "players";
            scoreboard.add(ChatColor.GREEN.toString() + m + ChatColor.GRAY + " " + word, 7);
            scoreboard.add(ChatColor.RED.toString(), 6);
        }

        scoreboard.update();
    }

    protected void setupPlayer(Player player) {
        PlayerUtil.resetPlayer(player);

        player.teleport(MPGame.getPlugin().getLobbyConfiguration().getWaitingLocation());
        scoreboard.send(player);
        if (timeUntilStart >= 0) player.setLevel(timeUntilStart);
    }

    private void tick() {
        if (timeUntilStart >= 0) {
            if (timeUntilStart == 0) {
                MPGame.getPlugin().getGameStateManager().changeState(MPGame.getPlugin().getGameModeInformation().get().getProvider().createFirstGameState(
                        MPGame.getPlugin().getSelectedMap()));
                MPGame.getPlugin().setGameStatus(GameStatus.IN_PROGRESS);
                tickTask.cancel();
                return;
            }

            Bukkit.getOnlinePlayers().forEach(p -> p.setLevel(timeUntilStart));
            updateScoreboard();

            if (timeUntilStart == 10 || timeUntilStart <= 5) {
                Bukkit.getOnlinePlayers().forEach(p -> {
                    p.sendMessage("The game is starting in " + timeUntilStart + " seconds.");
                    p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 10, 2);
                });
            }

            timeUntilStart--;
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        setupPlayer(event.getPlayer());

        if (Bukkit.getOnlinePlayers().size() >= MPGame.getPlugin().getSelectedMap().getMinPlayers() &&
                timeUntilStart == -1) {
            timeUntilStart = MPGame.getPlugin().getLobbyConfiguration().getCountdown();
            Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage("The game is starting in " + timeUntilStart + " seconds."));
        } else if (Bukkit.getOnlinePlayers().size() == Bukkit.getMaxPlayers() && timeUntilStart > 15) {
            // Game full, advance the clock
            timeUntilStart = 15;
            Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage("Lobby full! Starting in 15 seconds."));
        }

        updateScoreboard();
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        if (Bukkit.getOnlinePlayers().size() < MPGame.getPlugin().getSelectedMap().getMinPlayers()) {
            timeUntilStart = -1;
            Bukkit.getOnlinePlayers().forEach(p -> p.setLevel(0));
        }

        updateScoreboard();
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        if (event.toWeatherState()) event.setCancelled(true);
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        event.setCancelled(true);
    }
}
