package io.mineparty.game.extras.states.endgame;

import io.mineparty.game.GameState;
import io.mineparty.game.MPGame;
import io.mineparty.game.extras.rewards.GameRewardManager;
import io.mineparty.lib.util.CenteredText;
import io.mineparty.lib.util.PlayerUtil;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import java.util.Collection;

@RequiredArgsConstructor
public class EndGameState implements GameState {
    private final WinnerFetcher winnerFetcher;
    private final GameRewardManager rewardManager;

    @Override
    public void onStart(GameState previous) {
        Collection<Player> players = winnerFetcher.getWinners();

        for (Player player : Bukkit.getOnlinePlayers()) {
            for (int i = 0; i < 25; i++) {
                player.sendMessage("");
            }

            if (players.contains(player)) {
                CenteredText.sendCenteredMessage(player, ChatColor.GREEN + ChatColor.BOLD.toString() + "YOU HAVE WON");
            } else {
                CenteredText.sendCenteredMessage(player, ChatColor.RED + ChatColor.BOLD.toString() + "BETTER LUCK NEXT TIME");
            }

            CenteredText.sendCenteredMessage(player, winnerFetcher.getDescription() + ChatColor.RESET + " has won!");

            for (int i = 0; i < 3; i++) {
                player.sendMessage("");
            }

            PlayerUtil.resetPlayer(player);
            // TODO: Hook into MPCosmetic
        }

        if (rewardManager != null) {
            Bukkit.getOnlinePlayers().forEach(p -> rewardManager.sendRewards(p, 10));
        }

        Bukkit.getScheduler().runTaskLater(MPGame.getPlugin(), Bukkit::shutdown, 20 * 10);
    }

    @Override
    public void onStop(GameState next) {

    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Please join this lobby later.");
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        if (event.toWeatherState()) event.setCancelled(true);
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        event.setCancelled(true);
    }
}
