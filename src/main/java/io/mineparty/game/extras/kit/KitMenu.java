package io.mineparty.game.extras.kit;

import io.mineparty.game.MPGame;
import io.mineparty.lib.ItemBuilder;
import io.mineparty.lib.gui.PlayerGUI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class KitMenu extends PlayerGUI {
    private static final String CHECK = "\u2714";
    private static final String NO = "\u2718";
    private final KitManager manager;

    private static int r9(int s) {
        return s - (s % 9) + 9;
    }

    public KitMenu(KitManager manager) {
        super(MPGame.getPlugin(), r9(manager.getKits().size()), p -> "Kits");
        this.manager = manager;
    }

    public void update() {
        clear();
        int neededSize = r9(manager.getKits().size());
        if (getSize() != neededSize) {
            resize(neededSize);
        }
        for (KitInfo info : manager.getKits()) {
            addItem(player -> {
                ItemBuilder builder = info.getItem().clone();
                info.getDescription().forEach(builder::addLore);
                builder.addLore("");

                if (info.getPurchaseUuid() == null) {
                    builder.addLore(ChatColor.WHITE + "You may equip this kit:");
                    builder.addLore(ChatColor.DARK_GREEN + CHECK + ChatColor.RESET + ChatColor.GRAY + " Free kit");
                } else {
                    builder.addLore(ChatColor.WHITE + "You can't equip this kit:");
                    builder.addLore(ChatColor.DARK_RED + NO + ChatColor.RESET + ChatColor.GRAY + " Purchased in shop");
                }

                builder.hideFlags(ItemBuilder.Flag.ATTRIBUTES);
                return builder;
            }, p -> {
                manager.setPlayerKit(p, info);
                Bukkit.getPlayer(p.getMinecraftUuid()).sendMessage(ChatColor.GOLD + info.getName() + ChatColor.GREEN + " kit selected!");
            });
        }
    }
}
