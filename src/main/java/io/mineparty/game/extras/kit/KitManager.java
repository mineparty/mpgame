package io.mineparty.game.extras.kit;

import com.google.common.collect.ImmutableList;
import io.mineparty.commons.core.MinePartyPlayer;
import lombok.NonNull;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.*;

public class KitManager {
    private final List<KitInfo> kits = new ArrayList<>();
    private final Map<UUID, KitInfo> playerKits = new HashMap<>();
    private final KitMenu menu = new KitMenu(this);

    public void addKit(@NonNull KitInfo info) {
        kits.add(info);
        menu.update();
    }

    public void setPlayerKit(@NonNull MinePartyPlayer p, @NonNull KitInfo info) {
        if (!kits.contains(info)) {
            throw new IllegalArgumentException("Kit is not registered!");
        }
        playerKits.put(p.getMinecraftUuid(), info);
    }

    public void applyKit(@NonNull MinePartyPlayer p) {
        Player player = Bukkit.getPlayer(p.getMinecraftUuid());
        if (player == null) {
            return;
        }

        KitInfo info = playerKits.get(p.getMinecraftUuid());
        if (info == null) {
            if (kits.isEmpty()) {
                throw new IllegalStateException("No kits registered!");
            }
            info = kits.stream().filter(KitInfo::isDefaultKit).findFirst().orElse(kits.get(0));
        }

        info.getOnApply().accept(player);
    }

    public void openMenu(@NonNull MinePartyPlayer player) {
        menu.open(player);
    }

    public List<KitInfo> getKits() {
        return ImmutableList.copyOf(kits);
    }
}
