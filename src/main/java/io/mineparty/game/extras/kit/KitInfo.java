package io.mineparty.game.extras.kit;

import io.mineparty.lib.ItemBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

@Value
@Builder
public class KitInfo {
    @NonNull
    private final String name;
    @NonNull
    private final List<String> description;
    @NonNull
    private final ItemBuilder item;
    @NonNull
    private final Consumer<Player> onApply;
    private boolean defaultKit;
    private final UUID purchaseUuid;
}
