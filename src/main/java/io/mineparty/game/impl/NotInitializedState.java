package io.mineparty.game.impl;

import io.mineparty.game.GameState;
import io.mineparty.game.GameStatus;
import io.mineparty.game.MPGame;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerLoginEvent;

public class NotInitializedState implements GameState {
    @Override
    public void onStart(GameState previous) {

    }

    @Override
    public void onStop(GameState next) {
        MPGame.getPlugin().setGameStatus(GameStatus.WAITING);
    }

    @EventHandler
    public void onPlayerLogin(PlayerLoginEvent event) {
        event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "This lobby has encountered an issue. Please try another lobby.");
    }
}
