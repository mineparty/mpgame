package io.mineparty.game;

public enum GameStatus {
    RESTARTING,
    WAITING,
    IN_PROGRESS,
    ERROR
}
