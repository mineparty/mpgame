package io.mineparty.game;

import org.bukkit.event.Listener;

public interface GameState extends Listener {
    void onStart(GameState previous);
    void onStop(GameState next);
}
