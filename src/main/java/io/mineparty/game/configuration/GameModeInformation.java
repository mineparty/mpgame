package io.mineparty.game.configuration;

import lombok.Value;
import org.bukkit.plugin.Plugin;

@Value
public class GameModeInformation {
    private final Plugin plugin;
    private final GameModeProvider provider;
}
