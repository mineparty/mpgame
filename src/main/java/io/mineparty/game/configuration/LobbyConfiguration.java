package io.mineparty.game.configuration;

import io.mineparty.game.MPGame;
import io.mineparty.game.util.WorldUtil;
import io.mineparty.lib.util.ConfigurationSectionUtil;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class LobbyConfiguration {
    @Getter
    private final Location waitingLocation;
    @Getter
    private final int countdown;
    @Getter
    private final List<MapConfiguration> maps = new ArrayList<>();
    @Getter
    private final File mapDirectory;

    public LobbyConfiguration(ConfigurationSection section) {
        ConfigurationSection waitSec = ConfigurationSectionUtil.checkSection(section, "waiting-location");
        waitingLocation = Location.deserialize(waitSec.getValues(false));
        WorldUtil.setupWorld(waitingLocation.getWorld());

        countdown = section.getInt("countdown", 60);

        File mapFolder = new File(MPGame.getPlugin().getDataFolder(), "maps");
        if (!mapFolder.exists() || !mapFolder.isDirectory()) {
            throw new RuntimeException("Unable to find maps. 'maps' directory is missing.");
        }

        File worldFile = new File(section.getString("map-directory"));
        if (!worldFile.exists() || !worldFile.isDirectory()) {
            throw new RuntimeException("Unable to find worlds. " + worldFile + " directory is missing.");
        }

        mapDirectory = worldFile;

        for (File file : mapFolder.listFiles(f -> f.getName().endsWith(".yml"))) {
            MPGame.getPlugin().getLogger().info("Loading map " + file.getName());
            try {
                YamlConfiguration configuration = YamlConfiguration.loadConfiguration(file);
                maps.add(MapConfiguration.of(configuration));
            } catch (Exception e) {
                MPGame.getPlugin().getLogger().log(Level.SEVERE, "Unable to load map " + file + ", skipping it...", e);
            }
        }
    }
}
