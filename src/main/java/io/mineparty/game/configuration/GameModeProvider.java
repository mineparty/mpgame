package io.mineparty.game.configuration;

import io.mineparty.game.GameState;
import org.bukkit.configuration.ConfigurationSection;

public interface GameModeProvider {
    Object deserializeGameData(ConfigurationSection section);
    GameState createFirstGameState(MapConfiguration configuration);
}
