package io.mineparty.game.configuration;

import com.google.common.base.Preconditions;
import io.mineparty.game.MPGame;
import lombok.*;
import org.bukkit.configuration.ConfigurationSection;

@Data
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class MapConfiguration {
    private final String name;
    private final String world;
    private final String author;
    private final int minPlayers;
    private final int maxPlayers;
    @Getter(value = AccessLevel.PRIVATE)
    @Setter(value = AccessLevel.PRIVATE)
    protected ConfigurationSection configuration;
    @Setter
    private Object gameData;
    @Setter(value = AccessLevel.PRIVATE)
    private boolean active = false;

    public void injectExtraData() {
        Preconditions.checkState(MPGame.getPlugin().getGameModeInformation().isPresent(), "game mode information missing");
        gameData = MPGame.getPlugin().getGameModeInformation().get().getProvider().deserializeGameData(configuration);
        active = true;
        configuration = null; // get rid of it
    }

    protected static MapConfiguration of(ConfigurationSection section) {
        String name = Preconditions.checkNotNull(section.getString("name"));
        String world = Preconditions.checkNotNull(section.getString("world"));
        String author = Preconditions.checkNotNull(section.getString("author"));
        int minPlayers = section.getInt("min-players", 8);
        int maxPlayers = section.getInt("max-players", 8);
        MapConfiguration configuration = new MapConfiguration(name, world, author, minPlayers, maxPlayers);
        configuration.configuration = section;
        return configuration;
    }
}
