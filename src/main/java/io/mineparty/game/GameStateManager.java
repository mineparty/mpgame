package io.mineparty.game;

import com.google.common.base.Preconditions;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;

import java.util.concurrent.atomic.AtomicBoolean;

public class GameStateManager {
    private GameState currentState;
    private final AtomicBoolean switching = new AtomicBoolean(false);

    public void changeState(GameState newState) {
        Preconditions.checkNotNull(newState, "currentState");
        Preconditions.checkState(switching.compareAndSet(false, true), "game state transition already in progress");

        try {
            if (currentState != null) {
                try {
                    currentState.onStop(newState);
                    HandlerList.unregisterAll(currentState);
                } catch (Exception e) {
                    throw new RuntimeException("Unable to stop state " + currentState, e);
                }
            }

            try {
                newState.onStart(currentState);
                if (MPGame.getPlugin().getGameModeInformation().isPresent()) {
                    Bukkit.getPluginManager().registerEvents(newState, MPGame.getPlugin().getGameModeInformation().get().getPlugin());
                } else {
                    Bukkit.getPluginManager().registerEvents(newState, MPGame.getPlugin());
                }
            } catch (Exception e) {
                throw new RuntimeException("Unable to start state " + newState, e);
            }

            currentState = newState;
        } finally {
            switching.set(false);
        }
    }

    public GameState getCurrentState() {
        return currentState;
    }
}
