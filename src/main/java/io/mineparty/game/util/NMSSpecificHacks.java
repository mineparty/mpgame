package io.mineparty.game.util;

import com.google.common.collect.ImmutableList;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.bukkit.Bukkit;

import java.lang.reflect.Field;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class NMSSpecificHacks {
    private static final List<String> SUPPORTED = ImmutableList.of("v1_9_R1");

    public static void setMaximumPlayerCount(int newMax) throws NoSuchFieldException, IllegalAccessException {
        String serverPackageName = Bukkit.getServer().getClass().getPackage().getName();
        String version = serverPackageName.substring(serverPackageName.lastIndexOf('.') + 1);

        if (!SUPPORTED.contains(version)) {
            throw new RuntimeException(version + " is not supported.");
        }

        Field playerListField = Bukkit.getServer().getClass().getDeclaredField("playerList");
        playerListField.setAccessible(true);
        Object dedicatedPlayerList = playerListField.get(Bukkit.getServer());
        Field maxPlayersField = dedicatedPlayerList.getClass().getSuperclass().getDeclaredField("maxPlayers");
        maxPlayersField.setAccessible(true);
        maxPlayersField.setInt(dedicatedPlayerList, newMax);
    }
}
