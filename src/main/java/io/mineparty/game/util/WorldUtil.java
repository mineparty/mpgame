package io.mineparty.game.util;

import org.bukkit.World;

public class WorldUtil {
    private WorldUtil() {

    }

    public static void setupWorld(World world) {
        world.setGameRuleValue("doDaylightCycle", "false");
        world.setTime(0);
        world.setStorm(false);
    }
}
