package io.mineparty.game.util;

import io.mineparty.game.MPGame;
import io.mineparty.game.configuration.MapConfiguration;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.io.File;
import java.io.IOException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MapSetup {
    public static void setupMap(MapConfiguration selectedMap) throws IOException {
        MPGame.getPlugin().getLogger().info("Copying world for selected map '" + selectedMap.getName() + "'...");

        File gameWorldDirectory = new File("game_world");
        if (gameWorldDirectory.exists()) {
            try {
                FileUtils.forceDelete(gameWorldDirectory);
            } catch (IOException e) {
                throw new IOException("Unable to delete 'game_world' directory.", e);
            }

            MPGame.getPlugin().getLogger().info("... deleted existing game world.");
        }

        File sourceDirectory = new File(MPGame.getPlugin().getLobbyConfiguration().getMapDirectory(), selectedMap.getWorld());
        try {
            FileUtils.copyDirectory(sourceDirectory, gameWorldDirectory);
        } catch (IOException e) {
            throw new IOException("Unable to copy new world '" + selectedMap.getWorld() + "' to game world directory.", e);
        }

        MPGame.getPlugin().getLogger().info("... world copied! Loading world...");

        World gameWorld = WorldCreator.name("game_world").createWorld();
        WorldUtil.setupWorld(gameWorld);

        try {
            NMSSpecificHacks.setMaximumPlayerCount(selectedMap.getMaxPlayers());
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException("Unable to set maximum player count", e);
        }
    }
}
