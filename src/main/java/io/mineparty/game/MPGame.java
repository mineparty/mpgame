package io.mineparty.game;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.mineparty.game.configuration.GameModeInformation;
import io.mineparty.game.configuration.LobbyConfiguration;
import io.mineparty.game.configuration.MapConfiguration;
import io.mineparty.game.impl.NotInitializedState;
import io.mineparty.game.util.MapSetup;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class MPGame extends JavaPlugin implements Listener {
    @Getter
    private static MPGame plugin;
    @Getter
    private GameStateManager gameStateManager = new GameStateManager();
    @Getter
    private Optional<GameModeInformation> gameModeInformation = Optional.empty();
    @Getter
    private LobbyConfiguration lobbyConfiguration;
    @Getter
    private MapConfiguration selectedMap;
    @Getter
    private World gameWorld;
    @Setter
    @Getter
    @NonNull
    private GameStatus gameStatus = GameStatus.RESTARTING;
    private final Gson gson = new Gson();

    public void setGameModeInformation(GameModeInformation gameModeInformation) {
        this.gameModeInformation = Optional.of(gameModeInformation);

        lobbyConfiguration.getMaps().forEach(MapConfiguration::injectExtraData);
    }

    @Override
    public void onLoad() {
        // Early initialization.
        plugin = this;
    }

    @Override
    public void onEnable() {
        lobbyConfiguration = new LobbyConfiguration(getConfig());

        getServer().getPluginManager().registerEvents(this, this);

        // Configure a map
        Random random = new Random();
        selectedMap = lobbyConfiguration.getMaps().get(random.nextInt(lobbyConfiguration.getMaps().size()));
        try {
            MapSetup.setupMap(selectedMap);
        } catch (IOException e) {
            getLogger().log(Level.SEVERE, "Can't set up map. Shutting down the server in 10 seconds...", e);

            try {
                Thread.sleep(TimeUnit.SECONDS.toMillis(10));
            } catch (InterruptedException e1) {
                // ignore
            }

            Bukkit.shutdown();
            return;
        }

        gameWorld = Bukkit.getWorld("game_world");
        gameStateManager.changeState(new NotInitializedState());
    }

    @EventHandler
    public void onWeatherChange(WeatherChangeEvent event) {
        if (event.toWeatherState()) event.setCancelled(true);
    }

    @EventHandler
    public void onFoodLevelChange(FoodLevelChangeEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void onServerListPing(ServerListPingEvent event) {
        JsonObject object = new JsonObject();
        object.addProperty("status", gameStatus.name().toLowerCase());
        object.addProperty("current_map", selectedMap.getName());
        event.setMotd(gson.toJson(object));
    }
}
